using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnSchoolApi.Controllers
{
  [Route("[controller]")]
  public class ExamplesController : Controller
  {
    [HttpGet("/")]
    public IActionResult Index()
    {
      return new ObjectResult("Hello World!");
    }
  }
}
