using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnSchoolApi.Models;

namespace OnSchoolApi.Controllers
{
  [Route("api/[controller]")]
  public class TodosController : Controller
  {
    private readonly TodoContext _context;

    public TodosController(TodoContext context)
    {
      _context = context;
    }

    [HttpGet]
    public IEnumerable<TodoItem> GetAll()
    {
      return _context.TodoItems.ToList();
    }

    [HttpGet("{id}", Name = "GetTodo")]
    public IActionResult GetById(long id)
    {
      var item = _context.TodoItems.FirstOrDefault(t => t.Id == id);
      if(item == null) { return NotFound(); }
      return new ObjectResult(item);
    }

    [HttpPost]
    public IActionResult Create([FromBody] TodoItem item)
    {
      _context.TodoItems.Add(item);
      _context.SaveChanges();

      return CreatedAtRoute("GetTodo", new { id = item.Id }, item);
    }
  }
}
