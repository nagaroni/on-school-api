using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Xunit;

namespace OnSchoolApi.Tests
{
    public class ExamplesRequest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public ExamplesRequest()
        {
          _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
          _client = _server.CreateClient();
        }

        [Fact]
        public async Task ReturnHelloWorld()
        {
          var response = await _client.GetAsync("/");
          response.EnsureSuccessStatusCode();
          var body = await response.Content.ReadAsStringAsync();

          Assert.Equal("Hello World!", body);
        }
    }
}
